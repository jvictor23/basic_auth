
let pg = require('pg');
/*String de conexao com o banco de dados*/
let conString = "postgres://postgres:root@localhost:5432/auth_basic";

let pool = new pg.Pool({
    connectionString: conString
});

/*Banco de dados realizando conexao*/
pool.on('connect',()=>{});

/*exportando campo das queries*/
module.exports={
    query:(text,params) => pool.query(text,params)
}