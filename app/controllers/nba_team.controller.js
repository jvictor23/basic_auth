const model = require('../models/nba_team.model');

module.exports={
    getAllTeams: async(req,res)=>{
        const nba_team = await model.getAllTeams();
        return res.send({data: nba_team});
    }
}