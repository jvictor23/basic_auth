const express = require('express');
const router = express.Router();
const controller = require('../controllers/nba_team.controller');

router.get("/allTeam",controller.getAllTeams);

module.exports = router;