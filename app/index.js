const express = require("express");
const app = express();
const nba_team_route = require('./routes/nba_team.route');
const auth_basic = require('./auth/auth');

app.use(auth_basic)

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(nba_team_route);

app.listen(3000);
