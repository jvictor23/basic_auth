const db = require('../database/database');

module.exports={
    getAllTeams: async ()=>{
        const {rows} = await db.query("SELECT * FROM nba_team");
        return rows;
    }
}