const repository = require('../repositories/nba_team.repository');
module.exports={
    getAllTeams: async ()=>{
       const nba_team = await repository.getAllTeams();
       return nba_team;
    }
}