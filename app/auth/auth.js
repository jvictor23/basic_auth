const authBasic = require('express-basic-auth');
function myAsyncAuthorizer(username, password, cb) {
    const name = "testauth";
    const pass = "test123456";
    if (username === name && password === pass)
        return cb(null, true)
    else
        return cb(null, false)

}

function getUnauthorizedResponse(req) {
    return req.auth
        ? ('Access denied')
        : 'No credentials provided'
}

module.exports = authBasic({
    authorizer: myAsyncAuthorizer,
    authorizeAsync: true,
    unauthorizedResponse: getUnauthorizedResponse
})